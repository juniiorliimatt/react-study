import { Link } from "react-router-dom";

export const Navbar = () => {
  return (
    <nav className="nav justify-content-center">
      <Link to="/entrar" className="nav-link active">
        Login
      </Link>

      <Link to="/pagina-inicial" className="nav-link">
        Dashboard
      </Link>
    </nav>
  );
};
