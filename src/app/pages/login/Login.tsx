import { useEffect, useRef, useState } from "react";
import { ButtonLogin } from "./components/ButtonLogin";
import { InputLogin } from "./components/InputLogin";

export const Login = () => {
  const inputSenhaRef = useRef<HTMLInputElement>(null);
  const buttonRef = useRef<HTMLButtonElement>(null);

  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  useEffect(() => {
    console.log(email);
  }, [email]);

  useEffect(() => {
    console.log(senha);
  }, [senha]);

  const handleEntrar = () => {
    console.log(email);
    console.log(senha);
  };

  const handleInput = (newValue: any) => setEmail(newValue);
  const handleSenha = (newValue: any) => setSenha(newValue);

  return (
    <div className="container">
      <form>
        <InputLogin
          label="Email"
          value={email}
          type="email"
          onChange={handleInput}
          onPressEnter={() => inputSenhaRef.current?.focus()}
        />

        <InputLogin
          label="Senha"
          value={senha}
          type="password"
          ref={inputSenhaRef}
          onChange={handleSenha}
          onPressEnter={() => buttonRef.current?.focus()}
        />

        <ButtonLogin type="button" onClick={handleEntrar}>
          Entrar
        </ButtonLogin>
      </form>
    </div>
  );
};
