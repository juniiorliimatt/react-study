import React from "react";

interface IButtonLoginProps {
  type?: "button" | "submit" | "reset";
  onClick: () => void;
}

export const ButtonLogin: React.FC<IButtonLoginProps> = ({
  type,
  onClick,
  children,
}) => {
  return (
    <button type={type} className="btn btn-primary" onClick={onClick}>
      {children}
    </button>
  );
};
