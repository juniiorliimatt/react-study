import React from "react";

interface IInputLoginProps {
  label: string;
  value: string;
  type?: string;

  onChange: (newValue: string) => void;
  onPressEnter?: () => void;
}

export const InputLogin = React.forwardRef<HTMLInputElement, IInputLoginProps>(
  (props, ref) => {
    return (
      <div className="mb-3">
        <label className="form-label">{props.label}</label>
        <input
          ref={ref}
          type={props.type}
          className="form-control"
          value={props.value}
          onChange={(e) => props.onChange(e.target.value)}
          onKeyDown={(e) =>
            e.key === "Enter"
              ? props.onPressEnter && props.onPressEnter()
              : undefined
          }
        />
      </div>
    );
  }
);
