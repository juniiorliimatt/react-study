import { useContext } from "react";
import { UsuarioLogadoContext } from "../../shared/contexts";

export const Dashboard = () => {
  const usuarioLogadoContext = useContext(UsuarioLogadoContext);

  return <p>{UsuarioLogadoContext.nomeDoUsuario}</p>;
};
