import { Route, Routes } from "react-router-dom";
import { Dashboard } from "../pages";
import { Login } from "../pages/login/Login";
import { NotFound } from "../pages/notfound/NotFound";

export const MyRoutes = () => {
  return (
    <Routes>
      <Route path="/pagina-inicial" element={<Dashboard />} />
      <Route path="/entrar" element={<Login />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};
