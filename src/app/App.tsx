import { MyRoutes } from "./routes";
import { UsuarioLogadoProvider } from "./shared/contexts";

export const App = () => {
  return (
    <UsuarioLogadoProvider>
      <MyRoutes />;
    </UsuarioLogadoProvider>
  );
};
